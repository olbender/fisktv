#!/bin/bash

if [ "$#" -ne 5 ]; then
  echo "Use as: $0 <PORT> <CERT> <KEY> <GIT_URL> <GIT_BRANCH>"
  exit
fi

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

git clone -b $5 $4
cd fisktv

/usr/bin/fisktv --crt ${2} --key ${3} --port ${1} --src ./web --verbose &

while /bin/true; do
  git pull
  sleep 5
done

